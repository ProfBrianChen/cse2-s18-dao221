/* Danielle Okun
CSE2 Lab02
February 2, 2018
Bicylce cyclometer: records time elsapsed in seconds and number of rotations of the front wheel */
public class Cyclometer {
  // main method:
  public static void main(String[] args) {
    // input data:
    int secsTrip1 = 480; // length of trip 1
    int secsTrip2 = 3220; // length of trip 2
    int countsTrip1 = 1561; // number of wheel rotations for trip 1
    int countsTrip2 = 9037; // number of wheel rotations for trip 2
    // intermediate variables:
    double wheelDiameter = 27.0; // value of wheel diameter
    double PI = 3.14159; // value of pi
    double feetPerMile = 5280; // how many feet are in a mile
    double inchesPerFoot = 12; // how many inches per foot
    double secondsPerMinute = 60; // how many seconds are in a minute
    // output data:
    double distanceTrip1, distanceTrip2, totalDistance; // stating variables as doubles
    // printing trip information:
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts."); // information for trip 1
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts."); // information for trip 2
    // calculations for distances:
    distanceTrip1 = countsTrip1*wheelDiameter*PI/inchesPerFoot/feetPerMile; // distance for trip 1, converted into miles
    distanceTrip2 = countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; // distance for trip 2, converted into miles
    totalDistance = distanceTrip1+distanceTrip2; // total distance of both trips in miles
    // printing output (distances of each trip)
    System.out.println("Trip 1 was "+distanceTrip1+" miles."); // print distance of trip 1 in miles
    System.out.println("Trip 2 was "+distanceTrip2+" miles."); // print distance of trip 2 in miles
    System.out.println("THe total distance was "+totalDistance+" miles."); // print total distance in miles
  } // end of main method
} // end of class