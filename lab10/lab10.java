/* Danielle Okun
CSE2 Spring 2018
Lab 10: Operating on row and column matricies represented by two dimensional arrays */

import java.util.Random;

public class lab10 {
	public static void main(String [] args) {
		int firstHeight, secondHeight, firstWidth, secondWidth; // Declare height and width variables
		// Generate random variables from 1 to 10:
		firstHeight = (int)(Math.random() * 5) + 2;
		secondHeight = (int)(Math.random() * 5) + 2;
		firstWidth = (int)(Math.random() * 5) + 2;
		secondWidth = (int)(Math.random() * 5) + 2;
		
		// Allocate arrays A, B, and C:
		int[][] A = new int[firstWidth][firstHeight];
		int[][] B = new int[firstWidth][firstHeight];
		int[][] C = new int[secondWidth][secondHeight];
		
		A = increasingMatrix(firstWidth, firstHeight, true);
		B = increasingMatrix(firstWidth, firstHeight, false);
		C = increasingMatrix(secondWidth, secondHeight, true);
		printMatrix(A, true);
		printMatrix(B, false);
		printMatrix(C, true);
	}
	public static int[][] increasingMatrix(int width, int height, boolean format) {
		int[][] array = new int[width][height];
		if (format) {
			int i = 0;
			for(int r = 0; r < array.length; r++) {
				for(int c = 0; c < array[0].length; c++) {
					array[r][c] = i;
					i++;
				}
			}
		}
		else {
			int i = 0;
			for(int c = 0; c < array[0].length; c++) {
				for(int r = 0; r < array.length; r++) {
					array[r][c] = i;
					i++;
				}
			}
		}
		return array;
	}
	public static void printMatrix(int [][] array, boolean format) {
		if (array == null) {
			System.out.println("The array was empty!");
		}
		else {
			if (format) {
				System.out.println("Generating an array with height " + array.length + " and width " + array[0].length + ": ");
				for(int r = 0; r < array.length; r++) {
					System.out.print("[ ");
					for(int c = 0; c < array[0].length; c++) {
						System.out.print(array[r][c] + " ");
					}
					System.out.println("]");
				}
			}
			else {
				System.out.print("The arary was column major. ");
				int[][] newArray = new int[array.length][array[0].length];
				newArray = translate(array);
			}
		}
	}
	public static int[][] translate(int[][] array) {
		System.out.println("Translating column major to row major: ");
		int row = array.length;
		int column = array[0].length;
		int [][] newArray = new int[row][column];
		for(int i = 0; i < row; i++) {
			for(int j = 0; j < array[i].length; j++) {
				newArray[i][j] = array[i][j];
			}
		}
		int [] linearArray = new int[row * column];
		for(int i = 0; i < row; i++) {
			for(int j = 0; j < array[i].length; j++) {
				linearArray[(j * row) + i] = newArray[i][j];
			}
		}
		for(int i = 0; i < row; i++) {
			for(int j = 0; j < array[i].length; j++) {
				newArray[i][j] = linearArray[((i * column) + j)];
			}
		}
		printMatrix(newArray, true);
		return newArray;
	}
	public static void addMatrix(int[][] a, boolean formata, int[][] b, boolean formatb) {
		if (a.length != b.length || a[0].length != b[0].length) {
			System.out.println("The arrays cannot be added!");
			return null;
		}
		else if (a.length == b.length && a[0].length == b[0].length) {
			if (b && !a) {
				a = translate(a);
			}
			else if (a && !b) {
				b = translate(b);
			}
			System.out.println("Adding two matricies: ");
			for(int i = 0; i < a.length; i++) {
				System.out.print("[ ");
				for(int j = 0; j < a[0].length; j++) {
					System.out.print(a[i][j] + " ");
				}
				System.out.println(" ]");
			}
			System.out.println("Plus: ");
			for(int i = 0; i < b.length; i++) {
				System.out.print("[ ");
				for(int j = 0; j < b[0].length; j++) {
					System.out.print(b[i][j] + " ");
				}
				System.out.println(" ]");
			}
			int[][] finalArray = new int[a.length][a[0].length];
			System.out.println("Equals: ");
			for(int i = 0; i < a.length; i++) {
				for(int j = 0; j < a[0].length; j++) {
					finalArray[i][j] = a[i][j] + b[i][j];
				}
			}
			printMatrix(finalArray);
		}
	}
}