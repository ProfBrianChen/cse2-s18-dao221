public class gPractice {
	public static void main(String [] args) {
		for (int i = 2; i <= 8; i += 2) {
			System.out.print("i:");
			for (int numSpaces = 1; numSpaces <= i; numSpaces += 2) {
				System.out.print(" ");
			}
			System.out.println(i);
		}
	}
}