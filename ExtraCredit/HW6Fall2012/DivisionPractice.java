import java.util.Scanner;

public class DivisionPractice {
	public static void main(String [] args) {
		Scanner myScan = new Scanner(System.in);
		
		int answer;
		
		do {
			int dividend = (int)(Math.random() * 100 + 1);
			int divisor = (int)(Math.random() * dividend + 1);
			System.out.println("The division problem is: " + dividend + "/" + divisor);
			
			System.out.print("What is the quotient? ");
			int quotient = myScan.nextInt();
			System.out.print("What is the remainder? ");
			int remainder = myScan.nextInt();
			
			int correctQuotient = dividend/divisor;
			int correctRemainder = dividend%divisor;
			
			if (quotient == correctQuotient && remainder == correctRemainder) {
				System.out.println("Your answers are correct. ");
			}
			else {
				System.out.println("The correct answers are a quotient of " + correctQuotient + " and a remainder of " + correctRemainder);
			}
			
			System.out.println();
			System.out.print("Enter 1 if you would like another problem, enter 0 if you want to end: ");
			answer = myScan.nextInt();
			System.out.println();
		} while (answer == 1);
		
		System.out.println("Goodbye.");
	}
}