public class ConvertWeights {
	public static void main(String [] args) {
		System.out.println("Kilograms \t Pounds \t | \t Pounds \t Kilograms");
		int kilo = 1;
		double poundsFactor = 2.2;
		int pounds = 20;
		double kiloFactor = 0.453;
		for (; kilo <=199; kilo += 2) {
			double poundsConverted = kilo * poundsFactor;
			double kilosConverted = pounds * kiloFactor;
			String poundsAsString = String.format("%,.1f", poundsConverted);
			String kilosAsString = String.format("%,.2f", kilosConverted);
			System.out.println(kilo + "\t\t " + poundsAsString + "\t\t | \t " + pounds + "\t\t " + kilosAsString);
			pounds += 5;
		}
	}
}