import java.util.Scanner;

public class OddsandEvens {
	public static void main(String [] args) {
		Scanner myScan = new Scanner(System.in);
		
		int numOdd = 0, numEven = 0, total = 0, numInts = 0;
		int newInt;
		
		System.out.println("Ener a sequence of integers, ending with 0 (Press enter after each number): ");
		newInt = myScan.nextInt();
		
		while (newInt != 0) {
			if (newInt%2 == 0) {
				numEven++;
			}
			else {
				numOdd++;
			}
			total += newInt;
			numInts++;
			newInt = myScan.nextInt();
		}
		double average = ((double)total / numInts);
		System.out.println("The number of odd integers is " + numOdd);
		System.out.println("The number of even integers is " + numEven);
		System.out.println("The total is " + total);
		System.out.println("The average is " + average);
	}
}