import java.util.Scanner;

public class ComputeFactorial {
	public static void main(String [] args) {
		Scanner myScan = new Scanner(System.in);
		
		String prompt = "Enter a small integer: ";
		System.out.print(prompt);
		while(!myScan.hasNextInt()) {
			String junkWord = myScan.next();
			System.out.print("Error. Not an intger. " + prompt);
		}
		
		int input = myScan.nextInt();
		long product = 1;
		
		for (int factor = input; factor > 0; factor --) {
			product *= factor;
		}
		System.out.println(input + "! is equal to " + product);
	}
}

