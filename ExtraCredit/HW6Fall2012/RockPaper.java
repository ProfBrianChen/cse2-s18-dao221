import java.util.Scanner;

public class RockPaper {
	public static void main(String [] args) {
		Scanner myScan = new Scanner(System.in);
		
		System.out.print("How many rounds? ");
		int numRounds = myScan.nextInt();
		String computerPick = "error";
		String yourPick = "error";
		String outcome;
		int numTied = 0, numWon = 0, numLost = 0;
		
		for (int counter = 0; counter < numRounds; counter++ ) {
			
			int randomPick = (int)(Math.random() * 3);
			System.out.println("Rock (0), paper (1), scissors (2): " + randomPick);
			switch (randomPick) {
				case 0: yourPick = "rock";
					break;
				case 1: yourPick = "paper";
					break;
				case 2: yourPick = "scissors";
					break;
				default: yourPick = "error";
			}
			
			int computerRandom = (int)(Math.random() * 3);
			switch (computerRandom) {
				case 0: computerPick = "rock";
					break;
				case 1: computerPick = "paper";
					break;
				case 2: computerPick = "scissors";
					break;
				default: computerPick = "error";
			}
			
			if (computerPick.equals(yourPick)) {
				outcome = "It is a draw.";
				numTied++;
			}
			else if ((computerPick.equals("rock") && yourPick.equals("scissors")) || (computerPick.equals("paper") && yourPick.equals("rock")) || (computerPick.equals("scissors") && (yourPick.equals("paper")))) {
				outcome = "The computer won.";
				numLost++;
			}
			else {
				outcome = "You won.";
				numWon++;
			}
			
			System.out.println("The computer is " + computerPick + ". You are " + yourPick + ". " + outcome);
			
		}
		System.out.println("Out of " + numRounds + " rounds, you won " + numWon + " times(s), tied " + numTied + " time(s), and lost " + numLost + " times(s).");
	}
}