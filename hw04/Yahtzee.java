/* Danielle Okun
CSE2 HW04
February 16, 2018
Yahtzee: preforms or recieves input for random roll and scores the roll*/

// Import scanner class:
import java.util.Scanner;
// Begin class:
public class Yahtzee {
  // Begin main method:
  public static void main (String[] args) {
    // Establish inputs:
    Scanner myScanner = new Scanner( System.in ); // Establish to scanner that you are taking input from
    int digit1, digit2, digit3, digit4, digit5; // Establish digit variables as integers
    int aces = 0, twos = 0, threes = 0, fours = 0, fives = 0, sixes = 0; // Establish dice values as variable names
    int aceValue, twoValue, threeValue, fourValue, fiveValue, sixValue; // Establish variables for how much the number of dice of this value are worth
    int totalSection1, totalSection1wBonus, totalSection2, grandTotal; // Establish variables for total values
    int threeKind, fourKind, fullHouse, smStraight, lgStraight, yahtzee, chance; // Establish variables for lower section
    
    System.out.print("Type '1' to generate random numbers, type '2' to enter a 5 digit number: "); // Ask user to generate random numbers or input them
    if(!myScanner.hasNextInt()) {
      System.out.println("Error: Input is not an integer."); // Print error message and ends program if user does not type an integer
      return;
    }
    int retrieveNumbers = myScanner.nextInt(); // Stores next integer into variable 'retrieveNumbers'
    
    if (retrieveNumbers == 1) { // Generate 6 random digits
      digit1 = (int) (Math.random() * 6) + 1;
      digit2 = (int) (Math.random() * 6) + 1;
      digit3 = (int) (Math.random() * 6) + 1;
      digit4 = (int) (Math.random() * 6) + 1;
      digit5 = (int) (Math.random() * 6) + 1;
    }
    else if (retrieveNumbers == 2) { // Input 5 digit number
      System.out.print("Enter 5 digit number representing the result of a specific roll: "); // Print statement asks for user's input
      if(!myScanner.hasNextInt()) {
        System.out.println("Error: Input is not an integer."); // Print error message and ends program if user does not type an integer
        return;
      }
      int ranNum = myScanner.nextInt(); // Stores next integer into variable 'ranNum'
      
      if (ranNum < 10000 || ranNum > 99999) { 
        System.out.println("Error: Not a five digit number."); // Print error message and ends program if user does not type a five digit integer
        return;
      } 
      
      // Find each digit:
      digit1 = ranNum % 10; // Value for first digit
      ranNum /= 10;
      digit2 = ranNum % 10; // Value for second digit
      ranNum /= 10;
      digit3 = ranNum % 10; // Value for third digit
      ranNum /= 10;
      digit4 = ranNum % 10; // Value for fourth digit
      ranNum /= 10;
      digit5 = ranNum % 10; // Value for fifth digit
      
      // Check to see if digits are valid numbers for a dice roll:
      if (digit1 < 1 || digit1 > 6) { 
        System.out.println("Error: One or more digits are an impossible value for dice roll.");
        return;
      } // if first digit is not a number between 1 and 6, inclusive, program ends
      else if (digit2 < 1 || digit2 > 6) {
        System.out.println("Error: One or more digits are an impossible value for dice roll.");
        return;
      } // if second digit is not a number between 1 and 6, inclusive, program ends
      else if (digit3 < 1 || digit3 > 6) {
        System.out.println("Error: One or more digits are an impossible value for dice roll.");
        return;
      } // if third digit is not a number between 1 and 6, inclusive, program ends
      else if (digit4 < 1 || digit4 > 6) {
        System.out.println("Error: One or more digits are an impossible value for dice roll.");
        return;
      } // if fourth digit is not a number between 1 and 6, inclusive, program ends
      else if (digit5 < 1 || digit5 > 6) {
        System.out.println("Error: One or more digits are an impossible value for dice roll.");
        return;
      } // if fifth digit is not a number between 1 and 6, inclusive, program ends
      
    }
    else {
      System.out.println("Error: Did not choose option 1 or 2.");
      return; // If user did not input a 1 or 2, system prints "Error." and program ends
    }
   
    // Upper section calculations:
    switch (digit1) { // Increase count for the value that digit 1 contains
      case 1: aces++; break;
      case 2: twos++; break;
      case 3: threes++; break;
      case 4: fours++; break;
      case 5: fives++; break;
      case 6: sixes++; break;
    }
    switch (digit2) { // Increase count for the value that digit 2 contains
      case 1: aces++; break;
      case 2: twos++; break;
      case 3: threes++; break;
      case 4: fours++; break;
      case 5: fives++; break;
      case 6: sixes++; break;
    }
    switch (digit3) { // Increase count for the value that digit 3 contains
      case 1: aces++; break;
      case 2: twos++; break;
      case 3: threes++; break;
      case 4: fours++; break;
      case 5: fives++; break;
      case 6: sixes++; break;
    }
    switch (digit4) { // Increase count for the value that digit 4 contains
      case 1: aces++; break;
      case 2: twos++; break;
      case 3: threes++; break;
      case 4: fours++; break;
      case 5: fives++; break;
      case 6: sixes++; break;
    }
    switch (digit5) { // Increase count for the value that digit 5 contains
      case 1: aces++; break;
      case 2: twos++; break;
      case 3: threes++; break;
      case 4: fours++; break;
      case 5: fives++; break;
      case 6: sixes++; break;
    }
    // Multiply number of times each value was rolled by the value to get the number of points:
    aceValue = aces * 1; // Aces only
    twoValue = twos * 2; // Twos only
    threeValue = threes * 3; // Threes only
    fourValue = fours * 4; // Fours only
    fiveValue = fives * 5; // Fives only
    sixValue = sixes * 6; // Sixes only
    totalSection1 = aceValue + twoValue + threeValue + fourValue + fiveValue + sixValue; // Total value of all section one points
    // Add bonus if points qualify:
    if (totalSection1 > 63) {
      totalSection1wBonus = totalSection1 + 35; // Bonus is 35 points
    }
    else {
      totalSection1wBonus = totalSection1; // Did not qualify for bonus, point value stays the same
    }
    
    // Lower section calculations:
    // Three of a kind:
    if (aces >= 3 || twos >= 3 || threes >= 3 || fours >= 3 || fives >= 3 || sixes >= 3) {
      threeKind = totalSection1; // If conditions of 3 of a kind are met, threeKind = total of all the dice
    }
    else {
      threeKind = 0; // If conditions of 3 of a kind are met, threeKind = 0 points
    }
    // Four of a kind:
    if (aces >= 4 || twos >= 4 || threes >= 4 || fours >= 4 || fives >= 4 || sixes >= 4) {
      fourKind = totalSection1; // If conditions of 4 of a kind are met, fourKind = total of all the dice
    }
    else {
      fourKind = 0; // If conditions of 4 of a kind are met, fourKind = 0 points
    }
    // Full house:
    if ((aces == 3 || twos == 3 || threes == 3 || fours == 3 || fives == 3 || sixes == 3) 
        && (aces == 2 || twos == 2 || threes == 2 || fours == 2 || fives == 2 || sixes == 2)) {
        fullHouse = 25; // If conditions for full house are met, fullHouse = 25 points
    }
    else {
      fullHouse = 0; // If conditions for full house are not met, fullHouse = 0 points
    }
    // Small stright:
    if (threes >= 1 && fours >= 1 && ((twos >= 1 && fives >= 1) || (aces >= 1 && twos >= 1) || (fives >= 1 && sixes >= 1))) {
      smStraight = 30; // If conditions for small straight are met, smStraight = 30 points
    }
    else {
      smStraight = 0; // If conditions for small straight are not met, smStraight = 0 points;
    }
    // Large straight:
    if ( twos >= 1 && threes >= 1 && fours >= 1 && fives >= 1 && (aces >= 1 || sixes >= 1)) {
      lgStraight = 40; // If conditions for large straight are met, lgStraight = 40 points
    }
    else {
      lgStraight = 0; // If conditions for large straight are not met, lgStraight = 0 points
    }
    // Yahtzee:
    if (aces == 5 || twos == 5 || threes == 5 || fours == 5 || fives == 5 || sixes == 5) {
      yahtzee = 50; // If there are 5 of a kind, yahtzee = 50 points
    }
    else {
      yahtzee = 0; // If there are 5 of a kind, yahtzee = 0 points
    }
    // Chance:
    chance = totalSection1; // Set chance equal to total value fo dice (upper section total)
    // Lower section total:
    totalSection2 = threeKind + fourKind + fullHouse + smStraight + lgStraight + yahtzee + chance; // Find lower section total and assign it to variable
    
    // Total score:
    grandTotal = totalSection1wBonus + totalSection2; // Find grand total and assign it to variable
    
    
    
    // Print outputs for all the values:
    System.out.println("------------------------------------"); // Formatting
    System.out.println("5 digit number: "  + digit1 + " " + digit2 + " " + digit3 + " " + digit4 + " " +digit5);
    System.out.println("------------------------------------");
    System.out.println("Aces: \t\t\t\t\t" + aceValue + "\nTwos: \t\t\t\t\t" + twoValue + "\nThrees:\t\t\t\t\t" + threeValue + "\nFours: \t\t\t\t\t" + fourValue + "\nFives: \t\t\t\t\t" + fiveValue + "\nSixes: \t\t\t\t\t" + sixValue);
    System.out.println("Total upper section: \t\t\t" + totalSection1);
    System.out.println("Total upper section after bonus:\t" + totalSection1wBonus);
    System.out.println("------------------------------------------"); // Formatting
    System.out.println("3 of a kind:  \t\t\t\t" + threeKind);
    System.out.println("4 of a kind:  \t\t\t\t" + fourKind);
    System.out.println("Full house:  \t\t\t\t" + fullHouse);
    System.out.println("Small Straight:\t\t\t\t" + smStraight);
    System.out.println("Large Straight:\t\t\t\t" + lgStraight);
    System.out.println("Yahtzee: \t\t\t\t" + yahtzee);
    System.out.println("Chance: \t\t\t\t" + chance);
    System.out.println("Lower section total: \t\t\t" + totalSection2);
    System.out.println("------------------------------------------"); // Formatting
    System.out.println("Grand total: \t\t\t\t" + grandTotal);
    
    
  } // End main method
} // End class