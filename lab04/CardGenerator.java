/* Danielle Okun
CSE2 Lab04
February 16, 2018
Card Generator: Picks random card from a deck of 52 playing cards */

// Begin Class:
public class CardGenerator {
  // Begin Main Method:
  public static void main(String[] args) {
    // Initialize variables:
    int randomNumber; // Variable where random number is stored
    String cardSuit; // Variable where card suit is stored
    String cardValue = "error"; // Variable where card value is stored
    
    // Pick random number:
    randomNumber = (int) (Math.random()* 52) + 1; // Picks a random number from 1 to 52 and stores it in variable called "randomNumber"
    
    // Assign suit:
    if (randomNumber<14) {
      cardSuit = "diamonds"; // If number is 1-13, card is a dimond
    }
    else if (14<=randomNumber && randomNumber<=26) {
      cardSuit = "clubs"; // If number is 14-26, card is a club
    }
    else if (27<=randomNumber && randomNumber<=39) {
      cardSuit = "hearts"; // If number is 27-39, card is a heart
    }
    else {
      cardSuit = "spades"; // If number is 40-52, card is a spade
    }
    
    // Assign card value:
    int remainderValue = randomNumber%13; // Find the remaider of the random number when divded by 13
  
    switch (remainderValue) { // Switch number of remainder to value of card as a string
      case 1: cardValue = "ace";
        break;
      case 2: cardValue = "two";
        break;
      case 3: cardValue = "three";
        break;
      case 4: cardValue = "four";
        break;
      case 5: cardValue = "five";
        break;
      case 6: cardValue = "six";
        break;
      case 7: cardValue = "seven";
        break;
      case 8: cardValue = "eight";
        break;
      case 9: cardValue = "nine";
        break;
      case 10: cardValue = "ten";
        break;
      case 11: cardValue = "jack";
        break;
      case 12: cardValue = "queen";
        break;
      case 13: cardValue = "king";
        break;
    } // End switch statement
    
    // Print suit and value of random card:
    System.out.println("You picked the " + cardValue + " of " +cardSuit +"."); // Prints statement for user
  } // End Main Method
} // End Class