/* Danielle Okun
CSE2 HW 07
3/26/18
StringAnalysis: Checks to see if all the characters in a string are letters */

import java.util.Scanner; // Import scanner class

public class StringAnalysis { // Begin class
  public static void main(String []args) { // Begin main method
    Scanner myScanner = new Scanner(System.in); // Declare scanner
    String characters, answer; // Declare  strings
    String answer1 = "yes", answer2 = "no"; // Declare and initialize strings
    boolean truthValue = false; // Declare boolean value
    System.out.print("Enter a string of characters: "); // Print statement prompts user for input
    characters = myScanner.next(); // Stores input into string "characters"
    System.out.print("Do you want to examine the whole string, 'yes' or 'no'? "); // Print statement asked user a yes or no question
    answer = myScanner.next(); // Stores answer in string, "answer"
    while (!answer.equals(answer1) && !answer.equals(answer2)) { // If user does not input yes or no continues until valid input
      System.out.print("Error. To answer, type 'yes' or 'no'. "); // Print statement says error and asks again
      answer = myScanner.next(); // Stores input into answer
    }
    if (answer.equals(answer1)) { // If answer is yes
      truthValue = characterCheck(characters); // Continue to method "characterCheck" that just accepts a string
    }
    else if (answer.equals(answer2)) { // If answer is no
      int numCharacters = inputCheck("How many characters would you like to examine? ", myScanner); // Continue to method "inputCheck"
      truthValue = characterCheck(characters, numCharacters); // Continue to method "characterCheck" that accepts a string and how many characters
    }
    if (truthValue == true) { // If final output is true
        System.out.println("All characters examined are letters."); // Prints this statement
    }
    else { // If final output is not true
        System.out.println("Not all characters examined are letters."); // Prints this statement
    }
  } // End main method
  public static boolean characterCheck(String inputWord) { // Method that just accepts a string
    boolean truthValue; // Declare boolean "truthValue"
    int i = 0; // Declare and initialize i to zero as a counter
    int n = inputWord.length(); // Declare and initialize n to word length
    do { // Do this once, then continue while condition is true
      char currentChar = inputWord.charAt(i++); // Pulls out a single character from the string and increments counter, i, by one
      if (('a' <= currentChar) && ('z' >= currentChar)) { // Checks if charcacter is a letter
        truthValue = true; // sets truthValue to true
      }
      else { // If character is not a letter
        truthValue = false; // sets truth value to false
      }
    } while ((truthValue == true) && (i < n)); // Conditions that must be true for loop to continue
    return truthValue; // Output truthValue
  } // End characterCheck method
  public static boolean characterCheck(String inputWord, int n) { // Method that accepts a string and an integer
    boolean truthValue; // Declare boolean "truthValue"
    int i = 0; // Declare and initialize i to zero as a counter
    int length = inputWord.length(); // Declare and initialize "length" to word legth
    if (n < length) { // If given integer is less than word length keep integer the same
      n = n;
    }
    else { // If given intger is not less than word length, set integer to length of word
      n = length;
    }
    do { // Do this once, then continue while condition is true
      char currentChar = inputWord.charAt(i++); // Pulls out a single character from the string and increments counter, i, by one
      if (('a' <= currentChar) && ('z' >= currentChar)) { // Checks if character is a letter
        truthValue = true; // Sets truthValue to true
      }
      else { // If character is not a letter
        truthValue = false; // Sets truthValue to false
      }
    } while ((truthValue == true) && (i < n)); // Conditions that must be true for loop to continue
   return truthValue; // Output truthValue
  } // End characterCheck method
  public static int inputCheck(String printStatement, Scanner myScanner) { // Begin inputCheck method, that accepts a string and scanner
    System.out.print(printStatement); // Prints the input string to prompt user to input a value
    while (!myScanner.hasNextInt()) { // Loop runs while input value is not an integer
       String junkWord = myScanner.next(); // Stores integer in string "junkWord" to discard
       System.out.print("Error. Enter an integer. " + printStatement); // Prints error, asks for an integer, and prints input string again to prompt the user
    } 
     int output = myScanner.nextInt(); // Stores integer in integer "output" 
     return output; // Outputs value of integer "output" as an integer
  } // End inputCheck method
} // End class