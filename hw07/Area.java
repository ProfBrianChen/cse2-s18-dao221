/* Danielle Okun
CSE2 HW 07
3/26/18
Area: Calculates the area of a shape of the
user's choosing with inputs they provide */

import java.util.Scanner; // Import scanner class

public class Area { // Begin class
  public static void main(String []args) { // Begin main method
    Scanner myScanner = new Scanner(System.in); // define scanner
    String shape1, shape2, shape3; // Define variables as strings
    double areaShape; // Define variable for area as double
    shape1 = "rectangle"; // Initialize string variables 
    shape2 = "triangle";
    shape3 = "circle";
    System.out.print("To choose shape, type: 'rectangle', 'triangle', or 'circle': "); // Print statement prompts user to input shape
    String shape = myScanner.next(); // String 'shape' accepts user input
    while (!shape.equals(shape1) && !shape.equals(shape2) && !shape.equals(shape3)) { // Program asks user to input shape again until it is a valid input
      System.out.print("Error. To choose shape, type: 'rectangle', 'triangle', or 'circle' without capital letters: ");
      shape = myScanner.next(); // Stores input into string "shape"
    }
    if (shape.equals(shape1)) { // If user input is rectangle, it runs the program for a rectangle
      double rectHeight = inputCheck("Enter value for height of rectangle: ", myScanner); // Initializes and checks variable in method "inputCheck"
      double rectWidth = inputCheck("Enter value for width of rectangle: ", myScanner); // Initializes and checks variable in method "inputCheck"
      areaShape = areaRect(rectHeight, rectWidth); // Inputs variables into method "areaRect" to calculate area
      System.out.println("The area of the rectangle is " + areaShape); // Prints area value
    }
    else if (shape.equals(shape2)) { // If user input is triangle, it runs the program for a triangle
      double triangleHeight = inputCheck("Enter value for height of triangle: ", myScanner); // Initializes and checks variable in method "inputCheck"
      double triangleBase = inputCheck("Enter value for base of triangle: ", myScanner); // Initializes and checks variable in method "inputCheck"
      areaShape = areaTriangle(triangleHeight, triangleBase);  // Inputs variables into method "areaTriangle" to calculate area
      System.out.println("The area of the triangle is " + areaShape); // Prints area value
    }
    else if (shape.equals(shape3)) { // If user input is circle, it runs the program for a circle
      double circleRad = inputCheck("Enter value for radius of circle: ", myScanner); // Initializes and checks variable in method "inputCheck"
      areaShape = areaCircle(circleRad);  // Inputs variables into method "areaCircle" to calculate area
      System.out.println("The area of the circle is " + areaShape); // Prints area value
    }
  } // End main method
  public static double areaRect(double a, double b) { // Method areaRect accepts two doubles
    double area = (a * b); // Calculates area of a rectangle
    return area; // Outputs value for area
  } // End method areaRect
  public static double areaTriangle(double a, double b) { // Method areaTriangle accepts two doubles
    double area = (a * b) / 2; // Calculates area of a triangle
    return area; // Outputs value for area
  } // End method areaTriangle
  public static double areaCircle(double a) { // Method areaCircle accepts a single dobule
    double area = (Math.PI)*(Math.pow(a,2)); // Calculates area of a cirlce
    return area; // Outputs value for area
  } // End method areaCircle
  public static double inputCheck(String printStatement, Scanner myScanner) { // Method inputCheck accepts a String and scanner 
    System.out.print(printStatement); // Prints prompt to ask user to input a value
    while (!myScanner.hasNextDouble()) { // If value is not a double
      String junkWord = myScanner.next(); // Stores value in a string to discard it
      System.out.print("Error. Not a double. " + printStatement); // Prints error and asks again until value is a double
    }
    double output = myScanner.nextDouble(); // Stores double in double "output"
    return output; // Outputs value for the double
  } // End method inputCheck
} // End class