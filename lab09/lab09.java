/* Danielle Okun
CSE2 Sprint 2018 Chen
Lab09: Passing arrays inside methods - use 4 different methods to copy, inverty, and print the arrays */

public class lab09 {
	public static void main(String []args) {
		int array0[] = new int[8];
		int array1[] = new int[8];
		int array2[] = new int[8];
		for (int i = 0; i < 8; i++) { // For loop runs 10 times to fill lenth of array inputed
			array0[i] = (int)(Math.random() * 9); // Generates random integer from 0 to 9 and assigns it to next value in array "num"
		}
		array1 = copy(array0);
		array2 = copy(array0);
		
		System.out.print("Array 0 in main method: ");
		print(array0);
		System.out.print("Array 1 in main method: ");
		print(array1);
		System.out.print("Array 2 in main method: ");
		print(array2);
		
		System.out.print("Array 0 in inverter:    ");
		inverter(array0);
		System.out.print("Array 0 in main method: ");
		print(array0);
		inverter2(array1);
		System.out.print("Array 1 in main method: ");
		print(array1);
		int array3[] = inverter2(array2);
		System.out.print("Array 3 in main method: ");
		print(array3);
	}
	public static int [] copy(int []array) {
		int[] newArray = new int[array.length]; // Declares and initializes new array to manipulate in method
		for(int i = 0; i < array.length; i++) { // For loop runs for length of original array
			newArray[i] = array[i]; // Assigns each value in orignal array to next value in new array
		}
		return newArray;
	}
	public static void inverter(int []array) {
		int arrayLength = array.length;
		for (int i = 0; i < arrayLength/2; i++) {
			int temp = array[i];
			array[i] = array[arrayLength - i - 1];
			array[arrayLength - i - 1] = temp;
		}
		print(array);
	}
	public static int [] inverter2(int []array) {
		int []copiedArray = new int[array.length];
		copiedArray = copy(array);
		int arrayLength = array.length;
		for (int i = 0; i < arrayLength/2; i++) {
			int temp = copiedArray[i];
			copiedArray[i] = copiedArray[arrayLength - i - 1];
			copiedArray[arrayLength - i - 1] = temp;
		}
		return copiedArray;
	}
	public static void print(int []array) {
		for (int u: array) { // Prints values of array "grades"
			System.out.print(u + " ");
		}
		System.out.println();
	}
}