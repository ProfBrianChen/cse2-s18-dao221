/* Danielle Okun
CSE2 HW 06
3/9/18
Argyle: Repeating argyle pattern of diamonds based on input recieved from the user */

import java.util.Scanner; // Import scanner class 

public class Argyle { // Begin Class
  public static void main(String[] args) { // Begin main method
    // Input:
    Scanner myScanner = new Scanner(System.in); // Establish where input is coming from
    int widthWindow = 0, height = 0, widthDiamonds = 0, widthStripe = 1; // Initialize input variables

    // Width of window input and check:
    do {
      if (widthWindow < 0) {
        System.out.print("Error. ");
      }
      System.out.print("Enter a positive integer for the width of viewing window in characters: ");
      while(!myScanner.hasNextInt()) {
        String junkWord = myScanner.next();
        System.out.print("Error. Enter a positive integer for the width of viewing window in characters: ");
      }
      widthWindow = myScanner.nextInt();
    } while (widthWindow < 0);

    // Hight of window input and check:
    do {
      if (height < 0) {
        System.out.print("Error. ");
      }
      System.out.print("Enter a positive integer for the height of viewing window in characters: ");
      while(!myScanner.hasNextInt()) {
        String junkWord = myScanner.next();
        System.out.print("Error. Enter a positive integer for the height of viewing window in characters: ");
      }
      height = myScanner.nextInt();      
    } while (height < 0);
    
    // Width of diamond input and check:
    do {
      if (widthDiamonds < 0) {
        System.out.print("Error. ");
      }
      System.out.print("Enter a positive integer for the width of the argyle diamonds: ");
      while(!myScanner.hasNextInt()) {
        String junkWord = myScanner.next();
        System.out.print("Error. Enter a positive integer for the width of the argyle diamonds: ");
      }
      widthDiamonds = myScanner.nextInt();
    } while (widthDiamonds < 0);
    
    // Center stripe input and check:
    do {
      if ((widthStripe%2 == 0) || (widthStripe < 1) || (widthStripe > (widthDiamonds/2))) {
        System.out.print("Error: ");
      }
      System.out.print("Enter a positive odd integer less than half of the the width of the center stripe: ");
      while(!myScanner.hasNextInt()) {
        String junkWord = myScanner.next();
        System.out.print("Error. Enter a positive odd integer less than half of the the width of the center stripe: ");
      }
      widthStripe = myScanner.nextInt();
    } while ((widthStripe%2 == 0) || (widthStripe < 1) || (widthStripe > (widthDiamonds/2)));

    // Character inputs:
    System.out.print("Enter a first character for the pattern fill: ");
    String temp1 = myScanner.next(); // Stores input
    char patternFill1 = temp1.charAt(0); // Takes first character to be the fill
    System.out.print("Enter a second character for the pattern fill: ");
    String temp2 = myScanner.next(); // Stores input
    char patternFill2 = temp2.charAt(0); // Takes first character to be the fill
    System.out.print("Enter a third character for the stripe fill: ");
    String temp3 = myScanner.next(); // Stores input
    char stripeFill = temp3.charAt(0); // Takes first character to be the fill

    
 // Output:
 int inew, diamondFill, backgroundFill, beginningFill, endFill, jnew, inew2; // Initialize calculation variables
 int patternSize = widthDiamonds * 2; // Calculation for repeating size
 int patternMdpt = widthDiamonds; // Rename variable for logic purposes
    // Outer for loop calculates pattern height:
    for (int i = 0; i < height; i++) {
      // Finds place in repeating pattern height wise:
      inew = i%patternSize;
      if (inew > patternMdpt) {
        inew = patternSize - inew;
      }
      // Initialize calculations:
      diamondFill = inew*2;
      backgroundFill = patternSize - diamondFill;
      beginningFill = backgroundFill/2;
      endFill = backgroundFill/2;
      // Inner for loop calculates pattern width:
      for (int j = 0; j < widthWindow; j++) {
        // Finds place in repeating pattern width wise:
        jnew = j%patternSize;
        if (jnew == 0) { // Resets values when pattern restarts
          diamondFill = inew*2;
          backgroundFill = patternSize - diamondFill;
          beginningFill = backgroundFill/2;
          endFill = backgroundFill/2;
        }
        boolean stripeBack, stripeBack1, stripeBack2, stripeForward, stripeForward1, stripeForward2; // Initialize boolean values to print 'X' in correct location
        // Find new 'inew' value for midpoint of 'X':
        if (i%patternSize > patternMdpt) {
          inew2 = patternSize - (i%patternSize) - 1;
        }
        else {
          inew2 = inew;
        }
        // Calculate range for stipe width:
        stripeBack1 = (jnew >= inew2 - widthStripe/2); // Lower bound for backslash half of x
        stripeForward1 = (jnew >= (patternSize - inew2 - 1 - widthStripe/2)); // Lower bound for front slash half of x
        stripeBack2 = (jnew <= inew2 + widthStripe/2); // Upper bound for backslash half of x
        stripeForward2 = (jnew <= (patternSize - inew2 - 1 + widthStripe/2)); // Upper bound for front slash half of x
        stripeBack = stripeBack1 && stripeBack2; // Range for backslash half of x
        stripeForward = stripeForward1 && stripeForward2; // Range for front slash half of x
        // Calculation before start of diamond:
        if (beginningFill > 0){
          // Prints stripe fill before start of diamond:
          if (stripeBack || stripeForward) {
            System.out.print(stripeFill);
          }
          //Prints pattern fill if there is no diamond or stripe pattern:
          else {
            System.out.print(patternFill1);            
          }
          beginningFill--;
        }
        // Calculation for diamond:
        else if (diamondFill > 0) {
          // Prints stripe fill in the diamond pattern:
          if (stripeBack || stripeForward) {
            System.out.print(stripeFill);
          }
          // Prints diamond fill if there is no stripe there:
          else {
            System.out.print(patternFill2);
          }
          diamondFill--;
        }
        // Calculation after end of diamond pattern:
        else if (endFill > 0) {
          // Prints stripe fill after end of diamond pattern:
          if (stripeBack || stripeForward) {
            System.out.print(stripeFill);
          }
          // Prints pattern fill if there is no diamond or stripe pattern:
          else {
            System.out.print(patternFill1);
          }
          endFill--;
        }
      }
      System.out.println(); // Starts new line of pattern
    }
  } // End of main method
} // End of class