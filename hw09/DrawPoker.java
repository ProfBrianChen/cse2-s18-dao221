/* Danielle Okun
CSE2 Spring 2018 Chen
DrawPoker: Shuffles a deck of cards and assigns a random hand to each player, then decides who wins. */

public class DrawPoker { // Begin class "DrawPoker"
	public static void main(String []args) { // Begin main method
		int[] cards = new int[52]; // Declare and initialize new array of integers "cards" to length of 52 
		for (int i = 0; i < 52; i++) { // For loops run from i = 0 to i = 51
			cards[i] = i; // Assign values of array to 0-51 in order
		}
		System.out.println(); // Print statement begins new line
		
		// Shuffle the contents of the array:
	  for (int i = 0; i < cards.length; i++) { // For loop runs from i = 0 to i = length of array "cards" - 1
	    int target = (int) (cards.length * Math.random()); // Find random member to swap with
	    // Swap the values:
	    int temp = cards[target]; // stores value in array temporarily
	    cards[target] = cards[i]; // stores current member of array
	    cards[i] = temp; // assigns randomly generated value to member in array
	  }
		
		// Assign each player their hand:
		int[] firstPlayer = new int[5]; // Declare and inizialize array "firstPlayer" to length of 5
		int[] secondPlayer = new int[5]; // Declare and inizialize array "secondPlayer" to length of 5
		int j = 0;
		int k = 0;
		for (int i = 0; i < 10; i++) {  // For loop runs from i = 0 to i = 9
      if (i%2 == 0) { // If remainder is zero
        firstPlayer[j] = cards[i]; // Assigns current number in array to next value in "firstPlayer"
        j++; // Increment j by 1
      }
      else { // If remainder is anything but zero
        secondPlayer[k] = cards[i]; // Assigns current number in aray to next value in "secondPlayer"
        k++;
      } // Increment k by 1
    }
		System.out.println();
		
		// Print hands:
    System.out.print("\nPlayer 1 hand: ");  // Print staement
		for(int u : firstPlayer) { // For loop runs for length of array 
			System.out.print(u + " "); // Prints next value of array then a space
		}
		System.out.print("\nPlayer 2 hand: "); // Print statement
		for(int u : secondPlayer) { // For loop runs for length of array
			System.out.print(u + " "); // Prints next value of array then a space
		}
		
		// Print card numbers:
		int [] hand = new int[firstPlayer.length]; // Declare and initialize int array "hand" to length of "firstPlayer" array
		System.out.print("\n\nPlayer 1 cards by number: ");
		for(int i = 0; i < firstPlayer.length; i++) { // For loop runs from i = 0 to i = length of array -1
			hand[i] = firstPlayer[i]%13; // Finds remainder of array when divided by 13 and assigns it to next integer in array "hand"
			System.out.print(hand[i] + " "); // Prints integer and a space
		}
		int [] hand2 = new int[secondPlayer.length]; // Declares and initializes int array "hand2" to length of "secondPlayer" arary
		System.out.print("\nPlayer 2 cards by number: ");
		for(int i = 0; i < secondPlayer.length; i++) { // For loop runs from i = 0 to i = length of array -1
			hand2[i] = secondPlayer[i]%13; // Finds remainder of array when divided by 13 and assigns it to next integer in array "hand2"
			System.out.print(hand2[i] + " "); // Prints integer and a space
		}
		
		// Print card suits:
		int [] suit = new int[firstPlayer.length]; // Declares and initializes int array "suit" to length of "firstPlayer" array
		System.out.print("\n\nPlayer 1 card by suit: ");
		for(int i = 0; i < firstPlayer.length; i++) { // For loop runs from i = 0 to i = length of array -1
			suit[i] = firstPlayer[i]/13; // Frinds value at place "i" in array and divides it by 13, assigns that number to next value in "suit" array
			System.out.print(suit[i] + " "); // Prints integer and array
		}
		int [] suit2 = new int[secondPlayer.length]; // Declares and initializes int array "suit2" to length of "secondPlayer" array
		System.out.print("\nPlayer 2 card by suit: ");
		for(int i = 0; i < secondPlayer.length; i++) { // For loop runs from i = 0 to i = length of array -1
			suit2[i] = secondPlayer[i]/13; // Frinds value at place "i" in array and divides it by 13, assigns that number to next value in "suit2" array
			System.out.print(suit2[i] + " "); // Prints integer and array
		}
		System.out.println("\n");
		
		int firstPlayerPoints = 0;  // Declares and initializes counter "firstPlayerPoints" to zero
		int secondPlayerPoints = 0; // Declares and initializes counter "secondPlayerPoints" to zero
		
		// Check for pairs:
		boolean firstPair = pair(firstPlayer); // Declares and initializes boolean "firstPair" to result of method pair with input "secondPlayer"
		if (firstPair) {
			System.out.println("Player 1 has a pair.");
			firstPlayerPoints = 1 ;
		}
		else {
			System.out.println("Player 1 does not have a pair.");
		}
		boolean secondPair = pair(secondPlayer); // Declares nad initializes boolean "secondPair" to result of method pair with input "secondPlayer"
		if (secondPair) {
			System.out.println("Player 2 has a pair.");
			secondPlayerPoints = 1;
		}
		else {
			System.out.println("Player 2 does not have a pair.");
		}
		System.out.println();
		
		// Check for threeKind:
		boolean firstThreeKind = threeKind(firstPlayer); // Declares and initializes boolean "firstThreeKind" to result of method threeKind with input "firstPlayer"
		if (firstThreeKind) {
			System.out.println("Player 1 has three of a kind.");
			firstPlayerPoints = 2;
		}
		else {
			System.out.println("Player 1 does not have three of a kind.");
		}
		boolean secondThreeKind = threeKind(secondPlayer); // Declares and initializes boolean "secondThreeKind" to result of method threeKind with input "secondPlayer"
		if (secondThreeKind) {
			System.out.println("Player 2 has three of a kind.");
			secondPlayerPoints = 2;
		}
		else {
			System.out.println("Player 2 does not have three of a kind.");
		}
		System.out.println();
		
		// Check for flush:
		boolean firstFlush = flush(firstPlayer); // Declares and initializes boolean "firstFlush" to result of method flush with input "firstPlayer"
		if (firstFlush) {
			System.out.println("Player 1 has a flush.");
			firstPlayerPoints = 3;
		}
		else {
			System.out.println("Player 1 does not have a flush.");
		}
		boolean secondFlush = flush(secondPlayer);  // Declares and initializes boolean "secondFlush" to result of method flush with input "secondPlayer"
		if (secondFlush) {
			System.out.println("Player 2 has a flush.");
			secondPlayerPoints = 3;
		}
		else {
			System.out.println("Player 2 does not have a flush.");
		}
		System.out.println();
		
		// Check for full house:
		boolean firstFullHouse = fullHouse(firstPlayer);  // Declares and initializes boolean "firstFullHouse" to result of method fullHouse with input "firstPlayer"
		if (firstFullHouse) {
			System.out.println("Player 1 has a full house");
			firstPlayerPoints = 4;
		}
		else {
			System.out.println("Player 1 does not have a full house");
		}
		boolean secondFullHouse = fullHouse(secondPlayer);  // Declares and initializes boolean "secondFullHouse" to result of method fullHouse with input "secondPlayer"
		if (secondFullHouse) {
			System.out.println("Player 2 has a full house");
			secondPlayerPoints = 4;
		}
		else {
			System.out.println("Player 2 does not have a full house");
		}
		System.out.println("\n");
		
		// Find highest card of each hand if necessary, then print result:
		if (firstPlayerPoints > secondPlayerPoints) { // If first player had a better hand
			System.out.println("Player 1 won."); // Print result of game
		}
		else if (secondPlayerPoints > firstPlayerPoints) { // If second player had a better hand
			System.out.println("Player 2 won."); // Print result of game
		}
		else {
			int max1 = -1;
			int max2 = -1;
			for(int i = 0; i < hand.length; i++) { // Runs for loop for one less than length of array
				if (hand[i] > max1) { // If value is greater than the last
					max1 = hand[i]; // Sets max
				}
			}
			for(int i = 0; i < hand2.length; i++) { // Runs for loop for one less than length of array
				if (hand2[i] > max2) { // If value is greater than the last
					max2 = hand2[i]; // Sets max
				}
			}
			System.out.println("Player 1 max card: " + max1); // Prints string and greatest value in player's hand
			System.out.println("Player 2 max card: " + max2 + "\n"); // Prints string and greatest value in player's hand
			if (max1 > max2) {
				System.out.println("Player 1 won."); // Prints result of game
			}
			else if (max2 > max1) {
				System.out.println("Player 2 won."); // Prints result of game
			}
			else {
				System.out.println("Game is a tie"); // Prints result of game
			}
		}
		
  } // End main method
	
  public static boolean pair(int playerHand[]) { // Begin method "pair"
		int[] hand = new int[playerHand.length]; // Declares and initializes new array to manipulate in method
		for(int i = 0; i < playerHand.length; i++) { // For loop runs for length of original array
			hand[i] = playerHand[i]; // Assigns each value in orignal array to next value in new array
		}
		boolean pairPresent = false; // Declares and initializes boolean value to false
		for(int i = 0; i<hand.length; i++) { // For loop runs for lengh of the array
			hand[i] = hand[i]%13; // Find modulo of each number in array
		}
		for(int i = 0; i < hand.length; i++) { // Run for loop for length of array
			for(int j = i+1; j < hand.length; j++) { // Run for loop from value of i+1 to end of array
				if(hand[i] == hand[j]) { // If two value in the array are equivalent
					pairPresent = true; // Set boolean to true
				}
			}
		}
		return pairPresent; // Return boolean value
  } // End method "pair"
	
  public static boolean threeKind(int playerHand[]) { // Begin method "threeKind"
		int[] hand = new int[playerHand.length]; // Declares and initializes new array to manipulate in method
		for(int i = 0; i < playerHand.length; i++) { // For loop runs for length of original array
			hand[i] = playerHand[i]; // Assigns each value in orignal array to next value in new array
		}
    boolean threeKind = false; // Declares and initializes boolean value to false
		for(int i = 0; i<hand.length; i++) { // For loop runs for lengh of the array
			hand[i] = hand[i]%13; // Find modulo of each number in array
		}
		for(int i = 0; i < hand.length; i++) { // Run for loop for length of array
			for(int j = i+1; j < hand.length; j++) { // Run for loop from value of i+1 to end of array
				if(hand[i] == hand[j]) { // If two value in the array are equivalent
					for(int k = j+1; k < hand.length; k++) { // Runs for loop from value of j+1 to end of array
						if(hand[i] == hand[k]) { // If two values in the array are equal to a third value
							threeKind = true; // Set boolean to true
						} 
					}
				}
			}
		}
		return threeKind; // Return boolean value
  } // End method "threeKind
	
  public static boolean flush(int playerHand[]) { // Begin method "flush"
		int[] hand = new int[playerHand.length]; // Declares and initializes new array to manipulate in method
		for(int i = 0; i < playerHand.length; i++) { // For loop runs for length of original array
			hand[i] = playerHand[i]; // Assigns each value in orignal array to next value in new array
		}
		boolean flush = true; // Declares and initializes boolean value to true
		for(int i = 0; i < hand.length; i++) { // For loop runs for lengh of the array
			hand[i] = hand[i]/13; // Find division of number in array by 13 to find suit at indicated by a number
		}
		int i = 0;
		while (flush == true && i < hand.length - 1) { // While loop runs while conditions are true
			if ( hand[i] == hand [i+1]) { // If suit of card is equal to suit of next card
				i++; // Increments value of i by 1
			}
			else { 
				flush = false; // Assings flush to false
			}
		}
		return flush; // Returns boolean value
  } // End method "flush"
	
  public static boolean fullHouse(int playerHand[]) { // Begin method "fullHouse"
		int[] hand = new int[playerHand.length]; // Declares and initializes new array to manipulate in method
		for(int i = 0; i < playerHand.length; i++) { // For loop runs for length of original array
			hand[i] = playerHand[i]; // Assigns each value in orignal array to next value in new array
		}
		boolean fullHouse = false; // Declares and initializes boolean value to false
		for(int i = 0; i<hand.length; i++) { // For loop runs for lengh of the array
			hand[i] = hand[i]%13; // Find modulo of each number in array
		}
		for(int i = 0; i < hand.length; i++) { // Run for loop for length of array
			for(int j = i+1; j < hand.length; j++) { // Run for loop from value of i+1 to end of array
				if (hand[i] == hand [j]) { // If value of card is equal to value of next card
					for (int k = j + 1; k < hand.length; k++) { // Run for loop from value of j+1 to end of array
						if (hand[i] == hand[k]) { // If two values in array are equal to a third value
							for (int l = 0; l < hand.length; l++) { // Runs for loop for length of array
								for (int t = l + 1; t < hand.length; t++) { // Runs for loop from the value of l+1 to length of array
									if (hand[l] != hand[i] && hand[l] == hand[t]) { // If value is not equal to three of a kind but are equivalent
									fullHouse = true; // Sets boolean value to true
									}
								}
							}
						}
					}
				}
			}
		}
		return fullHouse; // Returns boolean value
  } // End method "fullHouse"
} // End class