/* Danielle Okun
CSE 02 Lab 05
March 2, 2018
TwistGenerator: Gets an input for "length" from user and checks to make sure it is a positive integer. Outputs a twist at the given length */

// Import scanner class:
import java.util.Scanner;

// Begin class:
public class TwistGenerator {
// Begin main method:
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); // Establish to scanner that you are taking input
    
    // Input:
    System.out.print("Input positive integer for twist length: "); // Print statement asks user to input an integer
    
    while (!myScanner.hasNextInt() ) { // Checks to see if the input is valid
      String junkWord = myScanner.next(); // Stores invalid input into string "junkWord"
      System.out.print("Invalid input. Input positive integer for twist length: "); // Print statement asks user to input an integer because their previous one was not valid
    }
    
    int length = myScanner.nextInt(); // Stores input into lenth
    
    while (length < 0) { // Checks to see if the input is positive
      System.out.print("Invalid input. Input POSITIVE integer for twist length: "); // Print statement asks user to input an integer because their previous one was not valid
      length = new Scanner(System.in).nextInt(); // Stores new input into this value
    }
    
   // Output:
   int count1 = 0; // Initialize counter as an integer named "count1"
   int count2 = 0; // Initialize counter as an integer named "count2"
   int count3 = 0; // Initialize counter as an integer named "count3"
  
    
   // Line one calculations:
   while (count1 < length) { // Runs this loop until it is equal to the length
     switch (count1%3) { // Figures out what to print next based on remainder
       case 0: System.out.print("\\");
         break;
       case 1: System.out.print(" ");
         break;
       case 2: System.out.print("/");
         break;
       default: System.out.print(count2%3);
         break;
     }
     count1++; // Increase count by 1 each time through the loop
   }
   System.out.println(); // Start new line
    
   // Line two calculations:
   while (count2 < length) { // Runs this loop until it is equal to the length
     switch (count2%3) { // Figures out what to print next based on remainder
       case 0: System.out.print(" ");
         break;
       case 1: System.out.print("X");
         break;
       case 2: System.out.print(" ");
         break;
       default: System.out.print(count2%3);
         break;
     }
     count2++; // Increase count by 1 each time through the loop
   }
   System.out.println(); // Start new line
    
   // Line three calculations:
   while (count3 < length) { // Runs this loop until it is equal to the length
     switch (count3%3) { // Figures out what to print next based on remainder
       case 0: System.out.print("/");
         break;
       case 1: System.out.print(" ");
         break;
       case 2: System.out.print("\\");
         break;
       default: System.out.print(count2%3);
         break;
     }
     count3++; // Increase count by 1 each time through the loop
   }
   System.out.println(); // Start new line
    
  } // End main method
} // End class