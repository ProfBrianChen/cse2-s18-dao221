/* Daniele OKun
CSE2 Spring 2018
RobotCity: Builds a city in a two dimensional array, displays it, invades it, displays it, and updates 5 times
*/

import java.util.Random; // Import random class

public class RobotCity { // Begin class
	public static void main(String[] args) { // Main method
		int row = (int)(Math.random() * 6) + 10; // Set int row equal to random int from 10 to 15
		int column = (int)(Math.random() * 6) + 10; // Set int column equal to random int from 10 to 15
		int[][] cityArray = new int[row][column]; // Declares two dimensional array "cityArray" and alocates space for values of row and column
		
		BuildCity(cityArray); // Runs "BuildCity" with input "cityArray"
		display(cityArray); // Runs "display" with input "cityArray"
		int k = (int)(Math.random() * 25) + 5; // Number of invading robots from 5 to 30
		invade(k, cityArray); // Runs "invade" with input int "k" and "cityArray"
		display(cityArray); // Runs "display" with input "cityArray"
		for(int i = 0; i < 5; i++) { // Runs loop 5 times
			update(cityArray); // Runs "invade" with input "cityArray"
			display(cityArray); // Runs "display" with input "cityArray"
		}
	} // End main method
	
	public static void BuildCity(int[][] city) { // Method BuildCity assigns value to the array
		for(int i = 0; i < city.length; i++) { // For loop runs for length of array "city"
			for(int j = 0; j < city[i].length; j++) { // For loop runs for length of array of "city[i]"
				city[i][j] = (int)(Math.random() * 900) + 100; // Assigns a random int from 100 to 999 to value in array
			} // The value is now also referenced from the array "cityArray" in the main method
		}
	} // End method BuildCity
	
	public static void display(int[][] city) { // Method display prints array
		for(int i = city.length - 1; i >= 0; i--) { // For loop runs for length of array, but decreases because city[0] starts at the south end
			for(int j = 0; j < city[i].length; j++) { // For loop runs for length of "city[i]"
				System.out.printf("%s ", city[i][j] + "\t"); // Prints current value with printf format
			}
			System.out.println(); // Returns print screen to new line
		}
		System.out.println(); // Prints a space inbetween cities
	} // End method display
	
	public static void invade(int k, int[][] city) { // Method invade generates random block coordinates where robots land and sets values to negative values
		for(int i = 0; i < k; i++) { // For loop runs for number of robots invading
			int row = (int)(Math.random() * (city.length - 1)); // Generates random row value based on number of rows in the array
			int column = (int)(Math.random() * (city[0].length - 1)); // Generates random column value based on number of columns in the array
			city[row][column] -= city[row][column] * 2; // Sets value at invaded block to negative of that value
		}
	} // End method invade
	
	public static void update(int[][] city) { // Method update moves the negative from its current position one to the right, the right most negatives dissapear
		for(int i = city.length - 1; i >= 0; i--) { // Runs for length of city array
			for(int j = city[i].length - 1; j >= 0; j--) { // Runs for length of city[i] array
				if (city[i][j] < 0) { // If current value is negative
					city[i][j] += (city[i][j] * -2); // Sets current value back to a positive number
					if ((j + 1) < city[i].length) { // If increasing j will not put array out of bounds
						city[i][j + 1] -= city[i][j + 1] * 2; // Sets value to the right to its negative value
					}
					//System.out.println("after: " + city[i][j] + " " + city[i][j+1]);
				}
			}
		}
	} // End method update
	
} // End class 