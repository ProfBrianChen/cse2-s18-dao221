/* Danielle Okun
CSE2 Lab08
Chen Spring 2018
Arrays: Asks for random number (from 5 to 10) of student names, then generates random numbers from 0 to 100 for their midterm grades */

import java.util.Scanner;
public class lab08 {
	public static void main(String [] args) {
		Scanner myScanner = new Scanner(System.in);
		String []students;
		int arraySize = (int)(Math.random() * 5) + 5;
		students = new String[arraySize];
		System.out.println("Enter " + arraySize + " student names (press enter after each one): ");
		for (int i = 0; i < arraySize; i++) {
			students[i] = myScanner.nextLine();
		}
		
		int []midterm;
		midterm = new int[arraySize];
		for(int j = 0; j < arraySize; j++) {
			midterm[j] = (int)(Math.random() * 100);
		}
		
		System.out.println("Here are the midterm grades of the " + arraySize + " students: ");
		for (int k = 0; k < arraySize; k ++) {
			System.out.println(students[k] + ": " + midterm[k]);
		}
		
	}
}