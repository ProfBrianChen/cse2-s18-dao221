/* Danielle Okun
CSE 02 Lab 03
February 9, 2018
Check: Recieves input of bill value, 
calculates tip, and splits bill evenly */

// Import scanner class:
import java.util.Scanner;
// Begin class:
public class Check { 
  // Main method:
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in ); // Establish to scanner that you are taking input
    
    // Input:
    System.out.print("Enter the original cost of the check in the form xx.xx: "); // Print statement tells user to input cost of the check
    double checkCost = myScanner.nextDouble(); // Stores cost of check in double with the name "checkCost"
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); // Print statement tells user to input how much tip they wish to give
    double tipPercent = myScanner.nextDouble(); // Stores tip percentage in double with the name "tipPercent"
    tipPercent /= 100; // Converts tip percentage value into a decimal value
    
    System.out.print("Enter the number of people who went out to dinner: "); // Print statement tells user to input how many ways they wish to split the check
    int numPeople = myScanner.nextInt(); // Stores number of people in an integer with the name "numPeople"
    
    // Calculations:
    double totalCost, costPerPerson; // Establish variables with these names as doubles
    int dollars, dimes, pennies; // Establish variables to store value of the cost, including digits to the right of the decimal point
    totalCost = checkCost*(1 + tipPercent); // Calculate the total cost of the check including tip
    costPerPerson = totalCost/numPeople; // Calculate the cost per person of the check including tip
    dollars = (int) costPerPerson; // Get the whole cost per person, without the decimal
    dimes = (int) (costPerPerson*10) % 10; // Get the "dimes" amount - value for the first digit after the decimal place
    pennies = (int) (costPerPerson*100) % 10; // Get the "pennies" amount - value for the second digit after the decimal place
    
    // Output:
    System.out.println("Each person in the group owes $"+ dollars + "." + dimes + pennies); // Output how much each person owes
    
  } // End of main method
} // End of class