/* Danielle Okun
CSE2 HW08 Program 2
Chen, Spring 2018
RemoveElements: Method generates a random array of 10 integers and deletes an integer or target integers based on user input
*/

import java.util.Random; // Import random class
import java.util.Scanner; // Import scanner class

public class RemoveElements { // Begin class RemoveElements
  public static void main(String [] arg){ // Begining of main method
		Scanner scan = new Scanner(System.in); // Declares and initializes scanner "scan"
		int num[] = new int[10]; // Declares array "num" and initializes it to a length of 10
		int newArray1[]; // Declares array of integers "newArray1"
		int newArray2[]; // Declares array of integers "newArray2"
		int index,target; // Declares variables "index" and "target"
		String answer = ""; // Declares and initializes string "answer"
		do {
			System.out.print("Random input 10 ints [0-9]. "); // Print statement lets user know that an array is beign generated
			num = randomInput(num); // Runs method "randomInput" with input of "num" and sets its ouput equal to array "num"
			String out = "The original array is:"; // Declares and initializes string "out"
			out += listArray(num); // Runs method "listArray" with input "num" and adds the output to existing string "out"
			System.out.println(out); // Prints string "out"
			
			System.out.print("Enter the index "); // Print statement prompts user to input an index
			index = scan.nextInt(); // Accepts next integer inputed to variable "index"
			newArray1 = delete(num,index); // Runs method "delete" with input "num" and "index" and sets output equal to "newArray1"
			String out1 = "The output array is "; // Declares and initializes string "out1"
			out1 += listArray(newArray1); // Runs method "listArray" with input "newArray1" and adds the output to existing string "out1"
			System.out.println(out1); // Prints string "out1"
			
			System.out.print("Enter the target value "); // Print statement prompts user to input a target value
			target = scan.nextInt(); // Accepts next integer inputed to variable "target"
			newArray2 = remove(num,target); // Runs method "remove" with input "num" and "target" and sets its output equal to array "newArray2"
			String out2 = "The output array is "; // Decalres and initializes string "out2"
			out2 += listArray(newArray2); // Runs method "listArray" with input "newArray2" and adds the output to existing string "out2"
			System.out.println(out2); // Prints string "out2"
			
			System.out.print("Go again? Enter 'y' or 'Y', anything else to quit- "); // Print statement prompts user to enter a character
			answer = scan.next(); // Accepts next string into string "answer"
		} while (answer.equals("Y") || answer.equals("y")); // While loop repeats if input matches
  } // End of main method
	
	public static int [] randomInput(int num[]) { // Begin method "randomInput" with input of int array "num" and output of an int array
		for (int i = 0; i < 10; i++) { // For loop runs 10 times to fill lenth of array inputed
			num[i] = (int)(Math.random() * 9); // Generates random integer from 0 to 9 and assigns it to next value in array "num"
		}
		return num; // Outputs array "num" to main method
	} // End of method "randomInput"
 
  public static String listArray(int num[]){ // Begin method "listArray" with input of array "num" and output of a string
		String out = "{"; // Declares and initializes string "out"
		for (int j = 0; j < num.length; j++){ // For loop runs once for each element of the array
			if (j > 0) { // Won't print the first time through
				out += ", "; // Adds comma and space to existing string "out"
			}
			out += num[j]; // Adds next element of array to existing string "out"
		}
		out += "} "; // Adds bracket and space to existing string "out"
		return out; // Outputs string "out" to main method
  } // End method "listArray"
	
	public static int [] delete(int [] list, int pos) { // Begin method "delete" with input of array "list" and int "pos" and output of an int array
		int arraySize = list.length; // Declares and initializes "arraySize" to length of array inputed
		int newList[] = new int[arraySize - 1]; // Declares and initializes new array "newList"
		for (int i = 0; i < arraySize - 1; i++) { // Begin for loop that runs one less time than the size of the array
			if (i < pos-1) { // If statement runs when position is less than target position minus 1
				newList[i] = list[i]; // Keep value in the same position from array "list" to array "newList"
			}
			else if (i >= pos-1) { // If statment runs when at target position or greater than target position
				newList[i] = list[i+1]; // Sets new array equal to the next value in the original array
			}
		}
		return newList; // Outputs array "newList" to main method
	} // End of method "delete"
	
	public static int [] remove(int [] list, int target) { // Begin method "remove" with input of array "list" and int "target" and output of an int array
		int arraySize = list.length; // Declare and initialize "arraySize" to size of inputed array
		int newList[] = new int[arraySize]; // Declare and initialize new array "newList"
		int j = 0; // Declare and initialize variable "j" to zero as a counter
		int numDeleted = 0; // Declare and initialize variable "numDeleted" to zero as a counter
		for (int i = 0; i < arraySize; i++) { // Begin for loop that will run for each element of the array
			if (list[i] != target) { // If statement runs if current integer in array is not equal to the one you are looking for
				newList[j] = list[i]; // Sets current value equal to next value in array "newList"
				j++; // Increments "j" to keep place in "newList"
			}
			else {
				numDeleted++; // If the if statement isn't true, "numDeleted" increments by 1 since there will be one less value in the final array
			}
		}
		int newArraySize = arraySize - numDeleted; // Declares and initializes "newArraySize" to the length of the final array
		int finalList[] = new int[newArraySize]; // Declares and initializes array "finalList" to "newArraySize"
		for (int i = 0; i < newArraySize; i++) { // For loop runs once for each value in "newArraySize"
			finalList[i] = newList[i]; // Sets values of "newList" equal to values of "finalList" so there are no zeros at the end
		}
		return finalList; // Outputs array "finalList" to main method
	} // End method "remove"
} // End class RemoveElements
