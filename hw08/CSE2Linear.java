/* Danielle Okun
CSE2 HW08 Program 1
Chen, Spring 2018
CSE2Linear: Takes 15 integers from user and preforms a binary search in the array, scambles the numbers, then preforms a linear search
*/

import java.util.Random; // Import random class
import java.util.Scanner; // Import scanner class

public class CSE2Linear { // Begin class
	public static void main(String []args) { // Begin main method
		
		Scanner myScanner = new Scanner(System.in); // Declare scanner as "myScanner"
		int[] grades; // Declare array "grades"
		grades = new int[15]; // Initialize length of array to 15
		System.out.println("Enter 15 ascending ints for final grades in CSE2: "); // Prompt user for 15 inputs
		
		for(int i = 0; i < 15; i ++) { // Loop 15 times, one for each input
			int value = inputCheck(myScanner); // Imports myScanner to method "inputCheck" and declares output to integer "value"
			while (value < 0 || value > 100 || (i > 0 && value < grades[i-1])) { // Checks to make sure values are valid
				if (value < 0 || value > 100) { // Prompts user again if this case
					System.out.println("Error. Value out of range. Enter integer greater than zero and less than 100. ");
					value = inputCheck(myScanner); // Checks input in method "inputCheck"
				}
				if (i > 0 && value < grades[i-1]) { // Prompts user again if this case
					System.out.println("Error. Value not greater than previous value. ");
					value = inputCheck(myScanner); // Checks input in method "inputCheck"
				} // Repeats while loop if one of these conditions still isn't true
			}
			grades[i] = value; // Stores final value of "value" as the next value in the array
		} // End for loop for inputs
		for (int u: grades) { // Prints values of array "grades"
			System.out.print(u + " ");
		}
		
		// Binary search for grade entered by user:
		System.out.print("\nEnter a grade to search for: "); // Prompts user to enter an integer
		int searchForBinary = inputCheck(myScanner); // Inputs user's input into method "inputCheck" to make sure it is in integer
		binarySearch(grades, searchForBinary); // Inputs user's input into method "binarySearch" to search for the grade
		
		// Shuffle the contents of the array:
		for (int i = 0; i<grades.length; i++) {
			int target = (int) (grades.length * Math.random()); // Find random member to swap with
			// Swap the values:
			int temp = grades[target]; // stores value in array temporarily
			grades[target] = grades[i]; // stores current member of array
			grades[i] = temp; // assigns randomly generated value to member in array
		}
		System.out.println("Scrambled: "); // Prints "Scrambled"
		for (int u: grades) { // Prints new array "grades"
			System.out.print(u + " ");
		}
		System.out.println();
		
		// Linear search for grade entered by user:
		System.out.print("Enter a grade to search for: "); // Prompts user for grade to search for
		int searchForLinear = inputCheck(myScanner); // Inputs user's input into method "inputCheck" to make sure input is valid
		linearSearch(grades, searchForLinear); // Inputs user's jinput into method "linearSearch" to search for the grade in the scrambled array
	}
	
	public static void binarySearch(int[] grades, int searchNum) { // Begin method "binarySearch" with no output, but inputs of an array and the number to search for
		int low = 0; // Declares and initializes original low to 0
		int high = 14; // Declares and initializes original high to 14
		int midPoint; // Declares variable "midPoint"
		int numIterations = 0; // Declares and initializes variable "numIterations" to 0 to count the number of iterations
		boolean valuePresent = false; // Declares and initializes boolean "valuePresent" to false
		do {
			midPoint = (low+high)/2; // Calculates new midPoint value each time through the do-while loop
			numIterations++; // Increments "numIterations" value by one each time through the do-while loop
			if (grades[midPoint] == searchNum) { // Checks to see if midPoint value is equal to the value being searched for
				valuePresent = true; // Sets "valuePresent" to true
				break; // Exits do-while loop
			}
			else if (grades[midPoint] > searchNum) { // If midPoint value is greater than the value being searched for
				high = midPoint - 1; // Changes value of "high"
			}
			else if (grades[midPoint] < searchNum) { // If midPoint value is less than the value being searched for
				low = midPoint + 1; // Changes value of "low"
			}
			if (low == high) { // If low value is equal to high value
				break; // Exits do-while loop, otherwise it would run forever
			}
		} while (grades[midPoint] != searchNum); // Repeats loop while searchNum is not found
		if (valuePresent) { // Runs if valuePresent is true
			System.out.println(searchNum + " was found in the list with " + numIterations + " iterations.");
		}
		else { // Runs if valuePresent is false
			System.out.println(searchNum + " was not found in the list with " + numIterations + " iterations.");
		}
	} // End method "binarySearch"
	
	public static void linearSearch(int [] grades, int searchNum) { // Begin method "linearSearch" with input of int array "grades" and int "searchNum"
		int position = -1; // Declares and initializes int "position" to -1
		int numIterations = 0; // Declares and initializes int "numIterations" to 0
		int i = 0; // Declares and initializes int "i" to 0 as a counter
		while (i < 15 && position == -1) { // While loop runs while conditions are true
			if (grades[i] == searchNum) { // If statement runs if value at position i is equal to number being searched for
				position = i; // Sets position to value of i to exit whle loop
			}
			numIterations++; // Increments numIterations by 1
			i++; // Increments i by 1
		}
		if (position != -1) { // If statement runs if position is not equal to -1
			System.out.println(searchNum + " was found in the list with " + numIterations + " iterations.");
		}
		else { // Else statement runs otherwise
			System.out.println(searchNum + " was not found in the list with " + numIterations + " iterations.");
		}
	} // End method "linearSearch"
	
	public static int inputCheck(Scanner myScanner) { // Begins method "inputCheck" with input of scanner "myScanner" and output of an int
		while (!myScanner.hasNextInt()) { // While statement runs if user input is not an integer
			String junkWork = myScanner.next(); // Stores invalid input in string to get rid of it
			System.out.println("Error. Value is not an integer. "); // Prints this line
		} // Repeats while input is not an integer
		int value = myScanner.nextInt(); // Stores integer value into int "value"
		return value; // Outputs "value"
	} // End method "inputCheck"
} // End class