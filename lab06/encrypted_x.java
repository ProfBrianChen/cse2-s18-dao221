/* Danielle Okun
CSE2 Lab 06
3/9/18
Encrypted X: Hidden X in characters */

import java.util.Scanner;

public class encrypted_x {
  public static void main(String[] args) {
  Scanner myScanner = new Scanner(System.in);
  
    System.out.print("Input integer between 0-100: ");
    while(!myScanner.hasNextInt()) {
      String junkWord = myScanner.next();
      System.out.print("Invalid input: input an intger between 0-100: ");
    }
    int input = myScanner.nextInt();
    while(input < 0 || input > 100) {
      System.out.print("Invalid input: input an intger between 0-100: ");
      input = new Scanner(System.in).nextInt();
    }
    
    for(int i = 0; i < input; i++) {
      for(int j = 0; j < input; j++) {
        if (i==j || j == (input - (i + 1))) {
          System.out.print(" ");
        }
        else {
          System.out.print("*");
        }
      }
      System.out.println();
    }
  }
}