/* Danielle Okun
CSE2 lab 07
3/26/18
Lab07: Random story generator  */

import java.util.Random;
import java.util.Scanner;

public class lab07 {
	public static String firstString() {
		Random randomGenerator = new Random();
		int randomInt1 = randomGenerator.nextInt(10);
		String randomWord = "<>";
		switch(randomInt1) {
			case 0: randomWord = "quick";
				break;
			case 1: randomWord = "aggressive";
				break;
			case 2: randomWord = "brave";
				break;
			case 3: randomWord = "calm";
				break;
			case 4: randomWord = "eager";
				break;
			case 5: randomWord = "small";
				break;
			case 6: randomWord = "large";
				break;
			case 7: randomWord = "old";
				break;
			case 8: randomWord = "slow";
				break;
			case 9: randomWord = "young";
				break;
		}
		return randomWord;
	}
		public static String secondString() {
		Random randomGenerator = new Random();
		int randomInt2 = randomGenerator.nextInt(10);
		String randomWord = "<>";
		switch(randomInt2) {
			case 0: randomWord = "dog";
				break;
			case 1: randomWord = "fox";
				break;
			case 2: randomWord = "cat";
				break;
			case 3: randomWord = "bird";
				break;
			case 4: randomWord = "fish";
				break;
			case 5: randomWord = "insect";
				break;
			case 6: randomWord = "turtle";
				break;
			case 7: randomWord = "ant";
				break;
			case 8: randomWord = "aligator";
				break;
			case 9: randomWord = "crab";
				break;
		}
		return randomWord;
	}
	public static String thirdString() {
		Random randomGenerator = new Random();
		int randomInt3 = randomGenerator.nextInt(10);
		String randomWord = "<>";
		switch(randomInt3) {
			case 0: randomWord = "carried";
				break;
			case 1: randomWord = "watched";
				break;
			case 2: randomWord = "bit";
				break;
			case 3: randomWord = "awoke";
				break;
			case 4: randomWord = "caught";
				break;
			case 5: randomWord = "passed";
				break;
			case 6: randomWord = "left";
				break;
			case 7: randomWord = "slept";
				break;
			case 8: randomWord = "swam";
				break;
			case 9: randomWord = "laid";
				break;
		}
		return randomWord;
	}
	public static String fourthString() {
		Random randomGenerator = new Random();
		int randomInt4 = randomGenerator.nextInt(10);
		String randomWord = "<>";
		switch(randomInt4) {
			case 0: randomWord = "owner";
				break;
			case 1: randomWord = "zooologist";
				break;
			case 2: randomWord = "kid";
				break;
			case 3: randomWord = "person";
				break;
			case 4: randomWord = "animal";
				break;
			case 5: randomWord = "pet";
				break;
			case 6: randomWord = "girl";
				break;
			case 7: randomWord = "boy";
				break;
			case 8: randomWord = "rock";
				break;
			case 9: randomWord = "house";
				break;
		}
		return randomWord;
	}
	public static void main (String []args) {
		Scanner myScanner = new Scanner(System.in);
		String randomWord1 = firstString();
		String randomWord2 = secondString();
		String randomWord3 = thirdString();
		String randomWord4 = fourthString();
		System.out.println("The " + randomWord1 + " " + randomWord2 + " " + randomWord3 + " the " + randomWord4 + ".");
		System.out.print("Would you like a new sentance? Type 0 for yes or 1 for no. ");
		int sentance = myScanner.nextInt();
		if (sentance == 0) {
			main(new String[] {});
		}
		else {
			return;
		}
	}
}
