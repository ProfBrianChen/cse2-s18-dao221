/* Danielle Okun
CSE2 HW03 Program #1
Convert inches of rain over acres into cubic miles */

// Import scanner class:
import java.util.Scanner;
// Begin class:
public class Convert {
  // Main method:
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in ); // Establish to scanner that you are taking input from
    
    // Input:
    System.out.print("Enter the effected area in acres: "); // Print statement tells user to input value for acres
    double acresCovered = myScanner.nextDouble(); // Stores number of acres in double with the name "acresCovered"
    
    System.out.print("Enter the rainfall in the affected area: "); // Print statement tells user to input value for rainfall in inches
    double rainfallArea = myScanner.nextDouble(); // Stores amount of rainfall in double with the name "rainfallArea"
    
    // Calulations:
    double gallonsRain = acresCovered*rainfallArea*27154; // Converts acres and inches into gallons and stores value into double called "gallonsRain"
    double cubicMiles = gallonsRain/1101117130711.3; // Converts gallons to cubic miles
    
    // Output:
    System.out.println(cubicMiles + " cubic miles"); // Prints cubic miles
    
   } // End of main method
}  // End of class