/* Danielle Okun
CSE02 HW03 Program #2
Pyramid: prompts the user for the dimensions of a pyramid
and returns the volue of the pyramid. */

// Import scanner class:
import java.util.Scanner;
// Begin class:
public class Pyramid {
  // Begin Main Method:
  public static void main(String[] args){
    Scanner myScanner = new Scanner( System.in ); // Establish to scanner that you are taking input from
    
    // Input:
    System.out.print("The length of the square side of the pyramid is: "); // Print statement tells user to input the length
    double sideLength = myScanner.nextDouble(); // Stores length in double with the name "sideLength"
    
    System.out.print("The height of the pyramid is: "); // Print statement tells user to input the height
    double pyramidHeight = myScanner.nextDouble(); // Stores height in double called "pyramidHeight"
    
    // Calculations:
    double pyramidVolume = Math.pow(sideLength, 2); // Finds area of the base
    pyramidVolume *= pyramidHeight; // Multiplies area of base by height
    pyramidVolume /= 3; // Calculates volume of pyramid and stores in double called "pyramidVolume"
    
    // Output:
    System.out.println("The volume inside the pyramid is: " + pyramidVolume); // Prints volume of the pyramid
    
  } // End main method
} // End class
