/* Danielle Okun
CSE2 HW06
March 4, 2018
HW06: Infinie Loops asking for user input about their current class */

import java.util.Scanner; // Import scanner class

public class hw05 { // Begin class
  public static void main(String[] args) { // Begin main method
    Scanner myScanner = new Scanner(System.in); // Establish where you are taking input from
    
    // Input:
    // Course number:
    System.out.print("What is the course number? "); // Print statement asks user to input an integer
    while (!myScanner.hasNextInt() ) { // Checks to see if the input is valid
      String junkWord = myScanner.next(); // Stores invalid input into string "junkWord"
      System.out.print("Invalid input. Input integer for course number: "); // Print statement asks user to input an integer because their previous one was not valid
    }
    int courseNum = myScanner.nextInt(); // Stores input into courseNum
    String blankSpace = myScanner.nextLine(); // Allows next input to be read as a line
    
    // Department name:
    System.out.print("What is the department name? "); // Print statement asks user for department name
    while (!myScanner.hasNextLine() ) { // Checks to see if the input is valid
      String junkWord = myScanner.next(); // Stores invalid input into string "junkWord"
      System.out.print("Invalid input. Input department name: "); // Print statement asks user to input an integer because their previous one was not valid
    }
    String departmentName = myScanner.nextLine(); // Stores input into departmentName
    
    // Number of times a week:
    System.out.print("How many times a week does the class meet a week? "); // Print statement asks user for number of times it meets
    while (!myScanner.hasNextInt() ) { // Checks to see if the input is valid
      String junkWord = myScanner.next(); // Stores invalid input into string "junkWord"
      System.out.print("Invalid input. Input integer for number of times the class meets a week: "); // Print statement asks user to input an integer because their previous one was not valid
    }
    int amountClasses = myScanner.nextInt(); // Stores input into amountClasses
    String blankSpace2 = myScanner.nextLine(); // Allows next input to be read as a line
    
    // Time the class starts:
    System.out.print("What time does the class start? (Form: 00:00 AM or 00:00 PM) "); // Print statement asks user for time that the class starts
    while (!myScanner.hasNextLine() ) { // Checks to see if the input is valid
      String junkWord = myScanner.next(); // Stores invalid input into string "junkWord"
      System.out.print("Invalid input. Input what time the class starts: "); // Print statement asks user to input an integer because their previous one was not valid
    }
    String startTime = myScanner.nextLine(); // Stores input into startTime

    // Instructor name:
    System.out.print("What is the name of the instructor? "); // Print statement asks user for name of instructor
    while (!myScanner.hasNext() ) { // Checks to see if the input is valid
      String junkWord = myScanner.next(); // Stores invalid input into string "junkWord"
      System.out.print("Invalid input. Input name of instructor: "); // Print statement asks user to input an integer because their previous one was not valid
    }
    String instructorName = myScanner.next(); // Stores input into instructorName
    
    // Number of students:
    System.out.print("How many students are in the class? "); // Print statement asks user for number of students
    while (!myScanner.hasNextInt() ) { // Checks to see if the input is valid
      String junkWord = myScanner.next(); // Stores invalid input into string "junkWord"
      System.out.print("Invalid input. Input integer for number of students: "); // Print statement asks user to input an integer because their previous one was not valid
    }
    int numStudents = myScanner.nextInt(); // Stores input into numStudents
    
    //Output: Prints all of the given inputs
    System.out.println("Course Number: " + courseNum);
    System.out.println("Department: " + departmentName);
    System.out.println("Times per week: " + amountClasses);
    System.out.println("Start time: " + startTime);
    System.out.println("Instructor: " + instructorName);
    System.out.println("Number of students:" + numStudents);
    
  } // End main method
} // End class